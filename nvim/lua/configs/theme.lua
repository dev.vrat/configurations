-- Termguicolors must be set.
vim.opt.termguicolors = true

vim.g.tokyonight_style = "storm"
vim.g.tokyonight_terminal_colors = 1

vim.cmd('colorscheme tokyonight')
