local nvim_lsp = require("lspconfig")

local servers = {
		"bashls",
    "gopls",
		"csharp_ls",
		"tsserver"
}
-- Use a loop to conveniently call 'setup' on multiple servers
for _, server in ipairs(servers) do
    nvim_lsp[server].setup {
        settings = {
            gopls = {analyses = {unusedparams = false}, staticcheck = true},
            json = { format = {enabled = false} }
        },
        flags = {debounce_text_changes = 150}
    }
end
