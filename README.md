# Configurations
A set of user defined settings for a bunch of applications as well as for the operating system (RedHat based distros). Nothing fancy and most probably incomplete and just functional enough. It is going to be inactively maintained.

## Current Configuarations includes:
The following list includes the configurations for the features that aren't shipped out of the box.
* Nerd font (Fantasque Sans Mono) for ligatures (in Kitty and Neovim).

### For Neovim (OR Editor)
* Intellisense support for BASH, C#, Go and Typescript.
* Debugging client (one of the requirements for debugging functionality).

### For Font Installation
1. Just copy the 'fonts' folder inside ~/.local/share/ to install all the fonts at user level (not system-wide).
2. Run `fc-cache -v` to refresh the font cache (doesn't requires sudo privileges for user level).

## Important Links
* [Language Server Configurations](https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md)
* [Debug Adapters/Debuggers](https://github.com/mfussenegger/nvim-dap/wiki/Debug-Adapter-installation)


## ToDo's
* Install 'LSP_Lines' (v2) plugin for better diagnostics.
