#!/bin/bash

# -----------------------------------------------------------------------------
# TO-DO: Installs Nodejs (and NPM) on the host machine (with DNF package manager) if not exists.
# -----------------------------------------------------------------------------


# -----------------------------------------------------------------------------
# Script to install all the user defined language servers to be used by Neovim.
# -----------------------------------------------------------------------------
sudo dnf install -y golang-x-tools-gopls

sudo npm install -g bash-language-server

sudo npm install -g dockerfile-language-server-nodejs

sudo npm install -g typescript typescript-language-server

sudo npm install -g vim-language-server

dotnet tool install --gloabl csharp-ls

